package be.drenock.sudoku;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Grid {
    private static final int SQUARE_LINE_SIZE = 3;
    @SuppressWarnings("unused")
    private static final int SQUARE_COLUMN_SIZE = SQUARE_LINE_SIZE;
    private static final int SQUARE_NUMBERS = 3;
    private static final int SIZE = SQUARE_NUMBERS * SQUARE_LINE_SIZE;

    private final Cell[][] grid;

    public Grid(String gridRpz) {
        if (gridRpz == null) throw new IllegalArgumentException("La grille ne peut être null");

        grid = new Cell[SIZE][SIZE];
        fill(gridRpz);
    }

    public void solve() {
        System.out.println(grid);
    }

    private void fill(String gridRpz) {
        String[] linesRpz = gridRpz.split(System.lineSeparator());

        if (linesRpz.length != SIZE) throw new IllegalArgumentException("Le fichier en entrée ne contient pas " + SIZE + " lignes.");

        for (int lineCount = 0; lineCount < linesRpz.length; lineCount++) {
            String[] digitsRpz = linesRpz[lineCount].split(" ");
            if (digitsRpz.length != SIZE) throw new IllegalArgumentException("La ligne " + (lineCount + 1) + " du fichier en entrée ne contient pas " + SIZE + " symboles.");
            for (int columnCount = 0; columnCount < digitsRpz.length; columnCount++) {
                String digitRpz = digitsRpz[columnCount];
                if (isDigit(digitRpz)) {
                    grid[lineCount][columnCount] = new Cell(Integer.parseInt(digitRpz), true);
                } else if (!isUnderscore(digitRpz)) {
                    throw new IllegalArgumentException("Le symbole " + digitRpz + " n'est pas reconnu");
                }
            }
        }
    }

    public boolean isDigit(String digitRpz) {
        try {
            int digit = Integer.parseInt(digitRpz);
            return digit >= 1 && digit <= SIZE;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isUnderscore(String digitRpz) {
        return digitRpz.equals("_");
    }

    private Set<Integer> getLineNumbers(int lineCount) {
        return Stream.of(grid[lineCount])
                .map(Cell::number)
                .collect(Collectors.toSet());
    }

    private Set<Integer> getColumnNumbers(int columnCount) {
        return Arrays.stream(grid)
                .map(line -> line[columnCount])
                .map(Cell::number)
                .collect(Collectors.toSet());
    }

    private Set<Integer> getSquareNumbers(int lineCount, int columnCount) {
        int squareOriginLineCount = lineCount - (lineCount % SQUARE_LINE_SIZE);
        int squareOriginColumnCount = columnCount - (columnCount % SQUARE_LINE_SIZE);

        Set<Integer> squareNumbers = new HashSet<>();
        for (int squareLineCount = squareOriginLineCount;
             squareLineCount < squareOriginLineCount + SQUARE_LINE_SIZE;
             squareLineCount++) {
            for (int squareColumnCount = squareOriginColumnCount;
                 squareColumnCount < squareOriginColumnCount + SQUARE_LINE_SIZE;
                 squareColumnCount++) {
                squareNumbers.add(grid[squareLineCount][squareColumnCount].number());
            }
        }
        return squareNumbers;
    }

    @Override
    public String toString() {
        return "Grid{" +
                "grid=" + Arrays.deepToString(grid) +
                '}';
    }
}
