package be.drenock.sudoku;

public record Cell(int number, boolean isProvided) { }
