package be.drenock.sudoku;

import java.nio.file.Files;
import java.nio.file.Path;

public class Application {

    public static void main(String[] args) throws Exception {
        String gridRpz = Files.readString(Path.of(Application.class.getClassLoader().getResource("Grid").toURI()));

        Grid grid = new Grid(gridRpz);
        System.out.println(grid);
    }
}
